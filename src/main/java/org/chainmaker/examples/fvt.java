package org.chainmaker.examples;

import com.alibaba.fastjson.JSON;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import org.chainmaker.contracts.docker.java.pb.proto.BatchKey;
import org.chainmaker.contracts.docker.java.pb.proto.Response;
import org.chainmaker.contracts.docker.java.sandbox.*;
import org.chainmaker.contracts.docker.java.sandbox.annotaion.ContractMethod;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.KVResult;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.KeyHistoryKvIter;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.KeyModification;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.ResultSetKV;

import java.io.IOException;
import java.util.*;

// @Contract
public class fvt implements IContract {

    @ContractMethod
    public Response display() {
        return SDK.success("display successful");
    }

    @ContractMethod
    public Response putState() throws ContractException, IOException {
        Map<String, ByteString> args = SDK.getArgs();
        String getKey = args.get("key").toStringUtf8();
        String getField = args.get("field").toStringUtf8();
        String getValue = args.get("value").toStringUtf8();

        SDK.logW(getKey + getField + getValue);

        SDK.putState(getKey, getField, getValue);

        return SDK.success("put state successfully");
    }

    @ContractMethod
    public Response putStateByte() throws ContractException, IOException {
        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        String getField = args.get("field").toStringUtf8();
        byte[] getValue = args.get("value").toByteArray();

        SDK.putState(getKey, getField, getValue);

        return SDK.success("put state successfully");
    }

    @ContractMethod
    public Response putStateFromKey() throws ContractException, IOException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        String getValue = args.get("value").toStringUtf8();

        SDK.putState(getKey, getValue);

        return SDK.success("put state successfully");
    }

    @ContractMethod
    public Response putStateFromKeyByte() throws ContractException, IOException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        byte[] getValue = args.get("value").toByteArray();

        SDK.putState(getKey, getValue);

        return SDK.success("put state successfully");
    }

    @ContractMethod
    public Response getState() throws ContractException, IOException, InterruptedException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        String getField = args.get("field").toStringUtf8();

        byte[] bytes = SDK.getState(getKey, getField);
        String result = new String(bytes);
        return SDK.success(result);
    }

    @ContractMethod
    public Response getStateByte() throws ContractException, IOException, InterruptedException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        String getField = args.get("field").toStringUtf8();

        byte[] result = SDK.getState(getKey, getField);
        return SDK.success(result);
    }

    @ContractMethod
    public Response getStateFromKey() throws ContractException, IOException, InterruptedException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();

        byte[] bytes = SDK.getState(getKey);
        String result = new String(bytes);
        return SDK.success(result);
    }

    @ContractMethod
    public Response getStateFromKeyByte() throws ContractException, IOException, InterruptedException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();

        byte[] result = SDK.getState(getKey);
        return SDK.success(result);
    }


    @ContractMethod
    public Response delState() throws ContractException, IOException {
        Map<String, ByteString> args = SDK.getArgs();

        ByteString getKey = args.get("key");
        if (getKey == null) {
            return SDK.error("key not exist");
        }
        String key = getKey.toStringUtf8();

        ByteString getField = args.get("field");
        String field = getField == null ? "" : getField.toStringUtf8();

        SDK.delState(key, field);

        return SDK.success("delete successfully");
    }

    @ContractMethod
    public Response getTxTimestamp() throws ContractException, IOException {
        return SDK.success(SDK.getTxTimestamp());
    }

    @ContractMethod
    public Response getBlockHeight() throws ContractException, IOException {

        return SDK.success(String.valueOf(SDK.getBlockHeight()));
    }

    @ContractMethod
    public Response getTx() throws ContractException, IOException, InterruptedException {

        String txId = SDK.getParam("txid");

        Response tx = SDK.getTxInfo(txId);

        SDK.logI(String.valueOf(tx.getStatus()));
        SDK.logI(tx.getMessage());

        return SDK.success( tx.getPayload().toByteArray());
    }


    @ContractMethod
    public Response timeOut() throws InterruptedException {
        Thread.sleep(50*1000 );
        return SDK.success("success finish timeout");
    }

    @ContractMethod
    public Response crossContract() throws InvalidProtocolBufferException, InterruptedException {
        Map<String, ByteString> args = SDK.getArgs();

        String contractName = args.get("contract_name").toStringUtf8();

        String calledMethod = args.get("contract_method").toStringUtf8();

        Response response = SDK.callContract(contractName, calledMethod, args);

        SDK.emitEvent("cross contract", new String[] {"success"});

        return response;
    }

    @ContractMethod
    public Response constructData() throws IOException, ContractException {

        String[][] records =  {
            {"key1", "field1", "val"},
            {"key1", "field2", "val"},
            {"key1", "field23", "val"},
            {"key1", "field3", "val"},
            {"key2", "field1", "val"},
            {"key3", "field2", "val"},
            {"key33", "field2", "val"},
            {"key33", "field2", "val"},
            {"key4", "field3", "val"},
        };

        for (String[] record : records){
            SDK.putState(record[0], record[1], record[2]);
        }

        SDK.logD("debug construct success");
        SDK.logI("info construct success");
        SDK.logW("warn construct success");

        return SDK.success("construct success!");
    }


    @ContractMethod
    public Response kvIterator() throws InterruptedException, ContractException {

        ResultSetKV[] iteratorList = new ResultSetKV[4];

        // 能查询出 key2, key3, key33 三条数据
        ResultSetKV iterator =  SDK.newIterator("key2", "key4");
        iteratorList[0] = iterator;

        // 能查询出 field1, field2, field23 三条数据
        ResultSetKV iteratorWithField = SDK.newIteratorWithField("key1", "field1", "field3");

        iteratorList[1] = iteratorWithField;

        // 能查询出 key3, key33 两条数据
        ResultSetKV preWithKeyIterator = SDK.newIteratorPrefixWithKey("key3");

        iteratorList[2] = preWithKeyIterator;

        // 能查询出 field2, field23 三条数据
        ResultSetKV preWithKeyFieldIterator = SDK.newIteratorPrefixWithKeyField("key1", "field2");

        iteratorList[3] = preWithKeyFieldIterator;

        for (int i = 0; i < iteratorList.length; i++) {
            ResultSetKV iter = iteratorList[i];

            while(iter.hasNext()){
                KVResult result = iter.next();
                SDK.logI(JSON.toJSONString(result));
            }
            SDK.logI("iter " + i + "end");
            iter.close();
        }

        return SDK.success("iterator success!");
    }

    @ContractMethod
    public Response keyHistoryIter() throws ContractException, InterruptedException {
        Map<String, ByteString> args = SDK.getArgs();

        String key = args.get("key").toStringUtf8();

        String field = args.get("field").toStringUtf8();

        KeyHistoryKvIter iter = SDK.newHistoryKvIterForKey(key, field);

        while(iter.hasNext()){
            KeyModification result = iter.next();
            SDK.logW(JSON.toJSONString(result));
        }

        iter.close();

        return SDK.success("iterator success!");
    }

    @ContractMethod
    public Response getOrigin() throws ContractException, InterruptedException {
        String senderAddr = SDK.origin();
        return SDK.success(senderAddr);
    }

    @ContractMethod
    public Response getSender() throws ContractException, InterruptedException {
        String senderAddr = SDK.sender();
        return SDK.success(senderAddr);
    }

    @ContractMethod
    public Response getBatchState() throws ContractException, IOException, InterruptedException {
        List<BatchKey> batchKeys = new ArrayList<>(Arrays.asList(
                BatchKey.newBuilder().setKey("key1").setField("field1").build(),
                BatchKey.newBuilder().setKey("key2").setField("field2").build(),
                BatchKey.newBuilder().setKey("key3").setField("field2").build(),
                BatchKey.newBuilder().setKey("key4").setField("field2").build()
        ));


       List<BatchKey> gotKVs = SDK.getBatchState(batchKeys);

       StringBuilder result = new StringBuilder();

       for (BatchKey kv: gotKVs){
           result.append(kv.getKey()).append("#").append(kv.getField()).append("#").append(kv.getValue().toStringUtf8()).append(";");
       }

       return SDK.success(result.toString());
    }

    public static void main(String[] args) {
        Sandbox.serve(args, new fvt());
    }
}
