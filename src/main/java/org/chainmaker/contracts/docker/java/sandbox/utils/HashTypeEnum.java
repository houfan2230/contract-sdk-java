package org.chainmaker.contracts.docker.java.sandbox.utils;

public enum HashTypeEnum {
    // these values are the same with go sdk
    // attention: the type defined in go sdk is uint, but here is int. it works till now, may not work in the future
    HASH_TYPE_SM3(20, "SM3"),
    HASH_TYPE_SHA256(5, "SHA-256"),
    HASH_TYPE_SHA3_256(11, "SHA3-256");

    private final int typeValue;
    private final String type;


    HashTypeEnum(int i, String s) {
        typeValue = i;
        type = s;
    }

    public int getTypeValue() {return typeValue;}

    public String getType(){return type;}

    public static HashTypeEnum getEnum(int i){
        switch (i){
            case 5:
                return HASH_TYPE_SHA256;
            case 11:
                return HASH_TYPE_SHA3_256;
            default:
                return HASH_TYPE_SM3;
        }
    }

}
