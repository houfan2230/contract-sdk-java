package org.chainmaker.contracts.docker.java.sandbox;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMMessage;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMRpcGrpc;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GrpcStreamClient {
    protected final Logger logger = LoggerFactory.getLogger(GrpcStreamClient.class.getName());

    protected final ManagedChannel channel;

    protected final BlockingQueue<DockerVMMessage> msgQueue;

    protected final StreamObserver<DockerVMMessage> requestObserver;

    public GrpcStreamClient(String host, int port) {
        channel = ManagedChannelBuilder.forAddress(host, port)
                .maxInboundMessageSize(100 * 1024 * 1024)
                .usePlaintext()
                .build();

        DockerVMRpcGrpc.DockerVMRpcStub asyncStub = DockerVMRpcGrpc.newStub(channel);

        msgQueue = new LinkedBlockingQueue<>();

        StreamObserver<DockerVMMessage> responseObserver = new StreamObserver<DockerVMMessage>() {
            @Override
            public void onNext(DockerVMMessage msg) {
                logger.debug("received msg: {}", msg);
                msgQueue.add(handleMsg(msg));
            }

            @Override
            public void onError(Throwable t) {
                logger.warn("send msg Failed: {}", Status.fromThrowable(t));
                System.exit(-1);
            }

            @Override
            public void onCompleted() {
                logger.info( "Finished msg");
            }
        };

        requestObserver = asyncStub.dockerVMCommunicate(responseObserver);
    }

    protected DockerVMMessage handleMsg(DockerVMMessage msg){
        return msg;
    }


    public void sendMsg(DockerVMMessage requestMsg){
        try {
            requestObserver.onNext(requestMsg);
        } catch (RuntimeException e) {
            // Cancel RPC
            requestObserver.onError(e);
            logger.error("send msg failed: {}", requestMsg);
            throw e;
        }
    }

    public DockerVMMessage blockingGetMsg() throws InterruptedException {
        return msgQueue.take();
    }

    public DockerVMMessage unarySendMsg(DockerVMMessage requestMsg) throws InterruptedException {
        try {
            requestObserver.onNext(requestMsg);
        } catch (RuntimeException e) {
            // Cancel RPC
            requestObserver.onError(e);
            logger.error("send msg failed: {}", requestMsg);
            throw e;
        }

        return msgQueue.take();
    }


    public Integer getTxWaitingQueueSize(){
        return msgQueue.size();
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

}



