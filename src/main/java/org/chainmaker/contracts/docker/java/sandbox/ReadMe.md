# 使用Java进行智能合约开发

## 环境依赖

1. Java 版本
   
   openjdk 11

## 编写Java合约

### 引用合约contracts-java-sdk

```html
        <dependency>
            <groupId>org.chainmaker</groupId>
            <artifactId>contracts-java-sdk</artifactId>
            <version>1.0.0</version>
        </dependency>
```

### 示例代码

```java
package org.chainmaker.examples;

import org.chainmaker.contracts.docker.java.pb.proto.Response;
import org.chainmaker.contracts.docker.java.sandbox.IContract;
import org.chainmaker.contracts.docker.java.sandbox.ContractException;
import org.chainmaker.contracts.docker.java.sandbox.SDK;
import org.chainmaker.contracts.docker.java.sandbox.annotaion.ContractMethod;

import java.io.IOException;

public class fact implements IContract {

    public String name() {
        return "fact";
    }

    @ContractMethod
    public Response save() throws IOException, ContractException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        String getField = args.get("field").toStringUtf8();
        String getValue = args.get("value").toStringUtf8();

        SDK.getInstance().logW(getKey + getField + getValue);

        SDK.getInstance().putState(getKey, getField, getValue);

        return SDK.success("put state successfully");
    }

    @ContractMethod
    public Response get() throws IOException, InterruptedException, ContractException {

        Map<String, ByteString> args = SDK.getArgs();

        String getKey = args.get("key").toStringUtf8();
        String getField = args.get("field").toStringUtf8();

        byte[] bytes = SDK.getInstance().getState(getKey, getField);
        String result = new String(bytes);
        return SDK.success(result);
    }


    public static void main(String[] args) {
        Sandbox.serve(args, new fact());
    }

}
```

### cmc 调用示例

```shell
#安装合约
./cmc client contract user create \
--contract-name=fact \
--runtime-type=JAVA \
--byte-code-path=/Users/ningzhouwei/Projects/contracts-sdk-java/target/fact.jar \
--version=1.0 \
--sdk-conf-path=./testdata/sdk_config.yml \
--admin-key-file-paths=./testdata/crypto-config/wx-org.chainmaker.org/user/admin1/admin1.tls.key \
--admin-crt-file-paths=./testdata/crypto-config/wx-org.chainmaker.org/user/admin1/admin1.tls.crt \
--sync-result=true \
--params="{}"


#调用合约
./cmc client contract user invoke \
--contract-name=fact \
--method=save \
--params="{\"key\":\"key0\",\"field\":\"field0\",\"value\":\"value00\"}" \
--sdk-conf-path=./testdata/sdk_config.yml \
--sync-result=true


./cmc client contract user invoke \
--contract-name=fact \
--method=get \
--params="{\"key\":\"key0\",\"field\":\"field0\"}" \
--sdk-conf-path=./testdata/sdk_config.yml \
--sync-result=true
```

### 编写合约注意事项

1. 合约必须实现接口 IContract

2. 代码入口，main函数的写法，将创建好的合约instance作为参数传给sanbox.serve
   
   ```java
       public static void main(String[] args) {
           Sandbox.serve(args, new fact());
       }
   ```

3. invoke的方法用@Action注解标识
   
   调用的方法名与函数名同名

4. 获取调用参数
   
   ```java
   Map<String, ByteString> args = SDK.getInstance().getArgs();
   
   String getKey = args.get("key").toStringUtf8();
   String getField = args.get("field").toStringUtf8();
   ```

5. 使用maven 打包合约，需要指明mainclass

```html
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-shade-plugin</artifactId>
    <version>3.4.0</version>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>shade</goal>
            </goals>
            <configuration>
                <artifactSet>
                    <includes>
                        <include>**</include>
                    </includes>
                </artifactSet>
                <transformers>
                    <transformer implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer"/>
                    <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                        <!-- 需要指明main class -->
                        <mainClass>org.chainmaker.examples.fact</mainClass>
                    </transformer>
                </transformers>
            </configuration>
        </execution>
    </executions>
```

5. 错误处理说明

可以直接throw错误，错误会作为合约执行结果返回。

### 

### ### 接口描述

待补充
