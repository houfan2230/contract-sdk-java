package org.chainmaker.contracts.docker.java.sandbox.kviterator;

import java.nio.charset.StandardCharsets;

public class KVResult {
    private final String key;

    private final String field;

    private final byte[] value;

    public KVResult(String key, String field, byte[] value) {
        this.key = key;
        this.field = field;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getField() {
        return field;
    }

    public byte[] getValue() {
        return value;
    }

    public String toString() {
        String valueString = new String(value, StandardCharsets.US_ASCII);
        return key + field + valueString;
    }

}
