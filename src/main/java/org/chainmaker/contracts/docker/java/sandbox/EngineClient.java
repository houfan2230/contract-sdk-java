package org.chainmaker.contracts.docker.java.sandbox;

import org.chainmaker.contracts.docker.java.pb.proto.CrossContext;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMMessage;
import org.chainmaker.contracts.docker.java.pb.proto.DockerVMType;

import org.chainmaker.contracts.docker.java.sandbox.utils.StepStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.chainmaker.contracts.docker.java.pb.proto.StepType;


public class EngineClient extends GrpcStreamClient {
    private final Logger logger = LoggerFactory.getLogger(EngineClient.class);

    public EngineClient(String host, int port) {
        super(host, port);
    }

    @Override
    protected DockerVMMessage handleMsg(DockerVMMessage msg){
        DockerVMMessage newMsg = StepStatistics.enterNextStep(msg, StepType.SANDBOX_GRPC_RECEIVE_TX_REQUEST, "");
        return newMsg;
    }

    public void registerSandbox(){
        DockerVMMessage registerMsg = DockerVMMessage.newBuilder().setType(DockerVMType.REGISTER)
                .setCrossContext(CrossContext.newBuilder().setProcessName(ConfigCtx.processName).build())
                .build();

        sendMsg(registerMsg);

    }
}



