package org.chainmaker.contracts.docker.java.sandbox.utils;

import org.chainmaker.contracts.docker.java.pb.proto.DockerVMType;

import java.time.Instant;

public class SysCallRecord {

    private DockerVMType opType;
    private long startTime;
    private long spentTime;

    public SysCallRecord(DockerVMType opType, long start) {
        this.opType = opType;
        this.startTime = start;
    }

    public SysCallRecord(DockerVMType opType) {
        this(opType, Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano());
    }

    public DockerVMType getOpType() {
        return opType;
    }

    public long getSpentTime() {
        return spentTime;
    }
    public void end() {
        long curTime = Instant.now().getEpochSecond() * 1_000_000_000 + Instant.now().getNano();
        this.spentTime = curTime - this.startTime;
    }

    public String toString() {
        return String.format("%s: start: %d, spend: %dμs", this.opType.name(), this.startTime, this.spentTime/1_000);
    }

}
