package org.chainmaker.contracts.docker.java.sandbox;

public class ContractException extends Exception {
    ContractException() {}

    public ContractException(String msg) { super(msg); }

}
