package org.chainmaker.contracts.docker.java.sandbox.utils;

import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.sandbox.ContractException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class ChainUtils {
    // these are defined in protocol package in golang
    // if the variable value or the method logic changed in golang, remember modify here
    public static final String ContractCreatorOrgIdParam = "__creator_org_id__";
    public static final String ContractCreatorRoleParam  = "__creator_role__";
    public static final String ContractCreatorPkParam    = "__creator_pk__";
    public static final String ContractSenderOrgIdParam  = "__sender_org_id__";
    public static final String ContractSenderRoleParam   = "__sender_role__";
    public static final String ContractSenderPkParam     = "__sender_pk__";
    public static final String ContractBlockHeightParam  = "__block_height__";
    public static final String ContractTxTimestamp = "__tx_time_stamp__";
    public static final String ContractCrossCallerParam  = "__cross_caller__";

    public static final String KeyCallContractResp = "KEY_CALL_CONTRACT_RESPONSE";

    public static final String KeyCallContractReq  = "KEY_CALL_CONTRACT_REQUEST";


    public static final String KeyIteratorFuncName = "KEY_ITERATOR_FUNC_NAME";
    public static final String KeyIterStartKey     = "KEY_ITERATOR_START_KEY";
    public static final String KeyIterStartField   = "KEY_ITERATOR_START_FIELD";
    public static final String KeyIterLimitKey     = "KEY_ITERATOR_LIMIT_KEY";
    public static final String KeyIterLimitField   = "KEY_ITERATOR_LIMIT_FIELD";
    public static final String KeyWriteMap         = "KEY_WRITE_MAP";
    public static final String KeyIteratorHasNext  = "KEY_ITERATOR_HAS_NEXT";

    public static final String KeyIterIndex = "KEY_KV_ITERATOR_INDEX";


    // stateKvIterator method
    public static final String FuncKvIteratorCreate    = "createKvIterator";
    public static final String FuncKvPreIteratorCreate = "createKvPreIterator";
    public static final String FuncKvIteratorHasNext   = "kvIteratorHasNext";
    public static final String FuncKvIteratorNext      = "kvIteratorNext";
    public static final String FuncKvIteratorClose     = "kvIteratorClose";

    // keyHistoryKvIterator method
    public static final String FuncKeyHistoryIterHasNext = "keyHistoryIterHasNext";
    public static final String FuncKeyHistoryIterNext    = "keyHistoryIterNext";
    public static final String FuncKeyHistoryIterClose   = "keyHistoryIterClose";


    public static final String KeyStateKey   = "KEY_STATE_KEY";
    public static final String KeyUserKey    = "KEY_USER_KEY";
    public static final String KeyUserField  = "KEY_USER_FIELD";
    public static final String KeyStateValue = "KEY_STATE_VALUE";


    public static final String KeyContractName     = "KEY_CONTRACT_NAME";

    public static final String KeyHistoryIterKey   = "KEY_HISTORY_ITERATOR_KEY";
    public static final String KeyHistoryIterField = "KEY_HISTORY_ITERATOR_FIELD";


    public static final String KeyTxId        = "KEY_TX_ID";
    public static final String KeyBlockHeight = "KEY_BLOCK_HEIGHT";
    public static final String KeyIsDelete    = "KEY_IS_DELETE";
    public static final String KeyTimestamp   = "KEY_TIMESTAMP";

    public static final String KeySenderAddr  = "KEY_SENDER_ADDR";


    public static final Set<String> SysParamKeys = new HashSet<>(Arrays.asList(ContractCreatorOrgIdParam,
            ContractCreatorRoleParam,
            ContractCreatorPkParam,
            ContractSenderOrgIdParam,
            ContractSenderRoleParam,
            ContractSenderPkParam,
            ContractBlockHeightParam,
            ContractTxTimestamp,
            ContractCrossCallerParam));

    public static final int DefaultMaxStateKeyLen = 1024;
    public static final String DefaultStateRegex = "^[a-zA-Z0-9._-]+$";

    public static void checkKeyFieldStr(String key, String field) throws ContractException {
        // Check field legality
        if (key.length() > DefaultMaxStateKeyLen ) {
            throw new ContractException(String.format("key[%s] too long", key));
        }

        if (!Pattern.matches(DefaultStateRegex, key)) {
            throw new ContractException(String.format("key[%s] can only consist of numbers, dot, letters and underscores", key));
        }

        // if field is "" then return
        if (field.length() == 0) {
            return;
        }

        // Check field legality
        if (field.length() > DefaultMaxStateKeyLen ) {
            throw new ContractException(String.format("key field[%s] too long", field));
        }

        if (!Pattern.matches(DefaultStateRegex, field)) {
            throw new ContractException(String.format("key field[%s] can only consist of numbers, dot, letters and underscores", field));
        }

    }

    public static byte[] leIntToByteArray(int i) {
        final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.putInt(i);
        return bb.array();
    }

    public static int byteArrayToLeInt(byte[] b) {
        final ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        return bb.getInt();
    }

    /**
     *
     * @param s ByteString
     * @return utf8 string default ""
     */
    public static String byteStringToUTF8String(ByteString s){
        return s == null ? "": s.toStringUtf8();
    }

}
