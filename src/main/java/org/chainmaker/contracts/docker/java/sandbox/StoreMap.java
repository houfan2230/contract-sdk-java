package org.chainmaker.contracts.docker.java.sandbox;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.KVResult;
import org.chainmaker.contracts.docker.java.sandbox.kviterator.ResultSetKV;
import org.chainmaker.contracts.docker.java.sandbox.utils.HashTypeEnum;

import java.io.IOException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;


public class StoreMap {
    // Name store map name
    @JSONField(ordinal = 1)
    private final String name;

    // Depth store map deep
    @JSONField(ordinal = 2)
    private final long depth;
    // HashType
    @JSONField(name="hash_type", ordinal = 3)
    private HashTypeEnum hashType;

    StoreMap(String name, long depth, HashTypeEnum hashType){
        this.name = name;
        this.depth = depth;
        this.hashType = hashType;
    }

    public String getName(){ return name; }
    public long getDepth(){
        return depth;
    }
    public int getHashType(){
        return hashType.getTypeValue();
    }

    @JSONField(name = "hash_type")
    public void setHashType(int code) {
        this.hashType = HashTypeEnum.getEnum(code);
    }

    public static StoreMap NewStoreMap(String name, long depth) throws ContractException, NoSuchAlgorithmException, IOException, InterruptedException {
        return NewStoreMap(name, depth, HashTypeEnum.HASH_TYPE_SM3);
    }

    public static StoreMap NewStoreMap(String name, long depth, HashTypeEnum hashType) throws ContractException, NoSuchAlgorithmException, IOException, InterruptedException {
        if (depth <= 0){
            throw new ContractException("depth must be greater than zero");
        }
        if (name.length() == 0) {
            throw new ContractException("name cannot be empty");
        }
        String stateKey = name + genHash(name, hashType);

        byte[] stateByte = SDK.getState(stateKey, "");

        if (stateByte.length != 0) {
            StoreMap storeMap = JSON.parseObject(stateByte, StoreMap.class);
            if (storeMap.getDepth() != depth) {
                throw new ContractException(String.format("storemap %s already exist, but depth is not equal", name));
            }
            return storeMap;
        } else {
            StoreMap newStoreMap = new StoreMap(name, depth, hashType);
            newStoreMap.save();
            return newStoreMap;
        }
    }

    private void save() throws NoSuchAlgorithmException, ContractException, IOException {
        byte[] storeMapBytes = JSON.toJSONBytes(this);
        String storeMapKey = name + genHash(name, hashType);
        SDK.putState(storeMapKey, "", storeMapBytes);
    }

    private static String genHash(String name, HashTypeEnum hashType) throws NoSuchAlgorithmException {
        Security.addProvider(new BouncyCastleProvider());
        StringBuilder sb = new StringBuilder();
        MessageDigest object = MessageDigest.getInstance(hashType.getType());
        byte[] encrypted = object.digest(name.getBytes());
        for (byte b : encrypted) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public byte[] get(String[] keys) throws ContractException, NoSuchAlgorithmException, IOException, InterruptedException {
        checkMapDepth(keys);

        KVResult kv = generateKey(keys);

        return SDK.getState(kv.getKey(), kv.getField());
    }

    // Set the value of Key in the StoreMap
    public void set(String[] keys, byte[] value) throws ContractException, IOException, NoSuchAlgorithmException {
        checkMapDepth(keys);
        KVResult kv = generateKey(keys);

        SDK.putState(kv.getKey(), kv.getField(), value);

    }

    // Del the Key in the StoreMap
    public void del(String[] keys) throws ContractException, IOException, NoSuchAlgorithmException {
        checkMapDepth(keys);
        KVResult kv = generateKey(keys);

        SDK.delState(kv.getKey(), kv.getField());
    }

    // Exist return whether the Key exist in the StoreMap
    public boolean exist(String[] keys) throws ContractException, IOException, NoSuchAlgorithmException, InterruptedException {
        checkMapDepth(keys);
        KVResult kv = generateKey(keys);

        byte[] ret = SDK.getState(kv.getKey(), kv.getField());

        return ret.length != 0;
    }

    // checkMapDepth
    private void checkMapDepth(String[] keys) throws ContractException {
        for (String k : keys) {
            if (k.length() == 0 ){
                throw new ContractException("key cannot be empty");
            }
        }
        if (keys.length != depth) {
            throw new ContractException("please check keys");
        }
    }

    private KVResult generateKey( String[] keys) throws NoSuchAlgorithmException {
        String field = genHash(name, hashType);
        for (String k : keys) {
            field = genHash(field+k, hashType);
        }

        return new KVResult(name+String.join("",keys), field, null);
    }

    public ResultSetKV newStoreMapIteratorPrefixWithKey(String[] keys) throws ContractException, InterruptedException {
        return SDK.newIteratorPrefixWithKey(name+String.join("",keys));
    }


}
