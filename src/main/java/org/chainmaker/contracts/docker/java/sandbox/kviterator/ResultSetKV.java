package org.chainmaker.contracts.docker.java.sandbox.kviterator;

import com.google.protobuf.ByteString;
import org.chainmaker.contracts.docker.java.pb.proto.*;
import org.chainmaker.contracts.docker.java.sandbox.*;
import org.chainmaker.contracts.docker.java.sandbox.utils.ChainUtils;

import java.util.HashMap;
import java.util.Map;

public class ResultSetKV {
    private final Context context;

    private final RuntimeClient runtimeClient;

    private final int index;

    public ResultSetKV(Context context, RuntimeClient runtimeClient, int index) {
        this.context = context;
        this.runtimeClient = runtimeClient;
        this.index = index;
    }

    public boolean hasNext() throws InterruptedException, ContractException {
        Map<String, ByteString> params = new HashMap<>(){{
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(ChainUtils.FuncKvIteratorHasNext));
            put(ChainUtils.KeyIterIndex, ByteString.copyFrom(ChainUtils.leIntToByteArray(index)));
        }};

        DockerVMMessage consumeKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CONSUME_KV_ITERATOR_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(SysCallMessage.newBuilder().putAllPayload(params).build())
                .build();

        DockerVMMessage result = runtimeClient.unarySendMsg(consumeKvIteratorReq);

        if (result.getSysCallMessage().getCode() == DockerVMCode.FAIL){
            throw new ContractException("get iterator has_next failed, msg: " + result.getSysCallMessage().getMessage());
        }

        ByteString payload = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyIteratorHasNext);

        int has = ChainUtils.byteArrayToLeInt(payload.toByteArray());

        return has != 0;
    }

    public void close() throws InterruptedException, ContractException {
        Map<String, ByteString> params = new HashMap<>(){{
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(ChainUtils.FuncKvIteratorClose));
            put(ChainUtils.KeyIterIndex, ByteString.copyFrom(ChainUtils.leIntToByteArray(index)));
        }};

        DockerVMMessage consumeKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CONSUME_KV_ITERATOR_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(SysCallMessage.newBuilder().putAllPayload(params).build())
                .build();

        DockerVMMessage result = runtimeClient.unarySendMsg(consumeKvIteratorReq);

        if (result.getSysCallMessage().getCode() == DockerVMCode.FAIL){
            throw new ContractException("close iterator failed, msg: " + result.getSysCallMessage().getMessage());
        }
    }


    public KVResult next() throws InterruptedException, ContractException {
        Map<String, ByteString> params = new HashMap<>(){{
            put(ChainUtils.KeyIteratorFuncName, ByteString.copyFromUtf8(ChainUtils.FuncKvIteratorNext));
            put(ChainUtils.KeyIterIndex, ByteString.copyFrom(ChainUtils.leIntToByteArray(index)));
        }};

        DockerVMMessage consumeKvIteratorReq = DockerVMMessage.newBuilder()
                .setChainId(context.getChainId())
                .setTxId(context.getTxId())
                .setType(DockerVMType.CONSUME_KV_ITERATOR_REQUEST)
                .setCrossContext(context.getCrossCtx())
                .setSysCallMessage(SysCallMessage.newBuilder().putAllPayload(params).build())
                .build();

        DockerVMMessage result = runtimeClient.unarySendMsg(consumeKvIteratorReq);

        if (result.getSysCallMessage().getCode() == DockerVMCode.FAIL){
            throw new ContractException("iterator get next row failed, msg: " + result.getSysCallMessage().getMessage());
        }

        String key = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyUserKey).toStringUtf8();
        String field = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyUserField).toStringUtf8();
        byte[] value = result.getSysCallMessage().getPayloadOrThrow(ChainUtils.KeyStateValue).toByteArray();

        return new KVResult(key, field, value);
    }

}
